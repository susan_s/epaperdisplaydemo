# README #

The firmware for the rapid prototype for the State of Charge display. The hardware consisted of an E-Ink Display 1.54" 200x200 (https://www.waveshare.com/wiki/1.54inch_e-Paper_Module) and an Arduino Uno. 

### Font edits made ###

The libraries\epd1in54\font8.c file was edited: it now contains a 50 pixel height Roboto font for numbers and "%" only. A new version of the libraries can be found here: https://www.waveshare.com/wiki/File:1.54inch_e-Paper_Module_code.7z