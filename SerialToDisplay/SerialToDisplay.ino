#include <SPI.h>
#include <epd1in54.h>
#include <epdpaint.h>

//#defines for screen

#define COLORED     0
#define UNCOLORED   1

//#defines for serial

#define COMMS_DELAY_MS    2000
#define COMMS_WAIT_MS     1000
#define SOM               0xA1
#define EOF_              0xAF
#define SEND_BYTE         0xAA

//Variables for screen

unsigned char image[1024];
Paint paint(image, 0, 0);    // width should be the multiple of 8
Epd epd;
unsigned long time_start_ms;
unsigned long time_now_s;
char SoCString[] = {'0', '0', '0', ' '};

//Variables for serial
uint8_t SocVal = 0;
char StringOfChoice[50] = {'d', 'e', 'f', 'a', 'u', 'l', 't'};
uint8_t data_array[50];
unsigned long commsDelayTimer = 0;
unsigned long commsWaitTimer = 0;

void setup() {
  //Serial
  Serial.begin(9600);
  //Screen
  if (epd.Init(lut_full_update) != 0) {
    Serial.print("e-Paper init failed");
    return;
  }
  ePaperClear();
  ePaperWriteInit();  //called twice for each memory section
  ePaperWriteInit();
}

void loop() {
  if (millis() - commsDelayTimer > COMMS_DELAY_MS) {
    SerialPoll();
    commsDelayTimer = millis();
  }
  delay(1000);
}

//Screen methods

void ePaperClear() {
  epd.ClearFrameMemory(0xFF);   // bit set = white, bit reset = black
  epd.DisplayFrame();
  epd.ClearFrameMemory(0xFF);   // bit set = white, bit reset = black
  epd.DisplayFrame();
}

void ePaperWriteInit() {
  paint.SetRotate(ROTATE_0);
  paint.SetWidth(200);
  paint.SetHeight(24);

  paint.Clear(UNCOLORED);
  paint.DrawStringAt(15, 0, " State of", &Font24, COLORED);
  epd.SetFrameMemory(paint.GetImage(), 0, 10, paint.GetWidth(), paint.GetHeight());

  paint.Clear(UNCOLORED);
  paint.DrawStringAt(15, 0, "  Charge", &Font24, COLORED);
  epd.SetFrameMemory(paint.GetImage(), 0, 40, paint.GetWidth(), paint.GetHeight());

  paint.SetWidth(110);
  paint.SetHeight(51);

  paint.Clear(UNCOLORED);
  paint.DrawStringAt(30, 2, SoCString, &Font8, COLORED);
  epd.SetFrameMemory(paint.GetImage(), 0, 80, paint.GetWidth(), paint.GetHeight());

  paint.Clear(UNCOLORED);
  paint.DrawStringAt(30, 2, "%", &Font8, COLORED);
  epd.SetFrameMemory(paint.GetImage(), 115, 80, paint.GetWidth(), paint.GetHeight());

  paint.SetWidth(200);
  paint.SetHeight(24);

  paint.Clear(UNCOLORED);
  paint.DrawStringAt(15, 0, StringOfChoice, &Font20, COLORED);
  epd.SetFrameMemory(paint.GetImage(), 0, 160, paint.GetWidth(), paint.GetHeight());

  epd.DisplayFrame();

  delay(2000);

  if (epd.Init(lut_partial_update) != 0) {
    Serial.print("e-Paper init failed");
    return;
  }
}

void ePaperUpdate() {

  SoCString[0] = SocVal / 100 + '0';
  SoCString[1] = SocVal / 10 + '0';
  SoCString[2] = SocVal % 10 + '0';

  if (SoCString[0] == '0') {
    SoCString[0] = ' ';
  }

  paint.SetWidth(110);
  paint.SetHeight(51);

  paint.Clear(UNCOLORED);
  paint.DrawStringAt(30, 2, SoCString, &Font8, COLORED);
  epd.SetFrameMemory(paint.GetImage(), 0, 80, paint.GetWidth(), paint.GetHeight());

  paint.SetWidth(200);
  paint.SetHeight(24);

  paint.Clear(UNCOLORED);
  paint.DrawStringAt(15, 0, StringOfChoice, &Font20, COLORED);
  epd.SetFrameMemory(paint.GetImage(), 0, 160, paint.GetWidth(), paint.GetHeight());
  epd.DisplayFrame();
}

//Serial Methods

void SerialPoll() {
  Serial.write(SEND_BYTE);
  commsWaitTimer = millis();
  while (millis() - commsWaitTimer < COMMS_WAIT_MS) {
    SerialRead();
  }
}

void SerialRead() {
  if (Serial.available()) {
    if (Serial.read() == SOM) {
      int i = 0;
      uint8_t data_array[30] = {};
      SocVal = Serial.read();
      Serial.println(SocVal);
      clearStr();
      while (Serial.available()) {
        data_array[i] = Serial.read();
        StringOfChoice[i] = char(data_array[i]);
        Serial.println("Add Info:" + String(StringOfChoice[i]));
        i++;
      }
      ePaperUpdate();
    }
    while (Serial.available()) {
      Serial.read();
    }
  }
}

void clearStr() {
  memset(StringOfChoice, ' ', sizeof(StringOfChoice));
}


